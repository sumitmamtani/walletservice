package com.bootcamp.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

class Wallet {
    @JsonProperty
    private int userId;
    @JsonProperty
    private int balance;

    Wallet(int userId, int balance) {
        this.userId = userId;
        this.balance = balance;
    }

    int fetchUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wallet wallet = (Wallet) o;
        return userId == wallet.userId &&
                balance == wallet.balance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, balance);
    }
}
