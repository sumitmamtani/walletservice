package com.bootcamp.wallet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
class WalletController {
    @Autowired
    private WalletService walletService;

    @GetMapping("/wallet")
    Wallet displayBalance(@RequestParam("userId") int userId){
        return walletService.displayBalance(userId);
    }

    @RequestMapping(value = "/walletxyz", method = RequestMethod.POST)
    @ResponseBody
    Wallet createWallet(@RequestBody Wallet wallet) {

        return  walletService.createWallet(wallet);}


}



