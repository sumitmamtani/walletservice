package com.bootcamp.wallet;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
class WalletService {
    private List<Wallet> userWallets;

    WalletService() {
        userWallets = new ArrayList<>();
        userWallets.add(new Wallet(1,100));
        userWallets.add(new Wallet(2, 200));
    }

    Wallet displayBalance(int userId){
        for(Wallet wallet:userWallets){
            if(wallet.fetchUserId() == userId){
                return wallet;
            }
        }
        return null;
    }
    Wallet createWallet(Wallet wallet){
        userWallets.add(wallet);
        return wallet;
    }
}
