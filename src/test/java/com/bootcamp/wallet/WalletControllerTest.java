package com.bootcamp.wallet;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WalletController.class)
class WalletControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletService walletService;

    @Test
    void testDisplayBalance() throws Exception {

        when(walletService.displayBalance(1)).thenReturn(new Wallet(1,100));

        mockMvc.perform(get("/wallet?userId=1"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"userId\":1,\"balance\":100}"));
    }
    @Test
    void testCreateWallet() throws Exception {

        when(walletService.createWallet(new Wallet(3,100))).thenReturn(new Wallet(3,100));

        mockMvc.perform(post("/walletxyz")
                .content("{\"userId\":3,\"balance\":100}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"userId\":3,\"balance\":100}"));
    }


}
